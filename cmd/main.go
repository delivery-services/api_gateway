package main

import (
	"api_gateway/api"
	"api_gateway/api/handler"
	"api_gateway/config"
	"api_gateway/grpc/client"
	"api_gateway/pkg/logger"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	services, err := client.NewGrpcClients(cfg)
	if err != nil {
		log.Error("Error while initializing grpc clients", logger.Error(err))
		return
	}

	h := handler.New(cfg, log, services)

	r := api.New(h)

	log.Info("api_gateway is running ...", logger.Any("port", cfg.HTTPPort))
	if err = r.Run(cfg.HTTPPort); err != nil {
		log.Error("Error while running server", logger.Error(err))
	}
}
