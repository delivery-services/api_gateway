package client

import (
	"api_gateway/config"
	pbc "api_gateway/genproto/delivery-protos/catalog_service"
	pbo "api_gateway/genproto/delivery-protos/order_service"
	pbu "api_gateway/genproto/delivery-protos/user_service"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	// CATALOG SERVICE
	CategoryService() pbc.CategoryServiceClient
	ProductService() pbc.ProductServiceClient

	// USER SERVICE
	ClientService() pbu.ClientServiceClient
	CourierService() pbu.CourierServiceClient
	UserService() pbu.UserServiceClient
	BranchService() pbu.BranchServiceClient
	AuthService() pbu.AuthServiceClient

	// ORDER SERVICE
	DeliveryTarifService() pbo.DeliveryTarifServicesClient
	OrderService() pbo.OrderServiceClient
	OrderProductService() pbo.OrderProductServiceClient
	AlternativeService() pbo.AlternativeServiceClient
}
type grpcClients struct {
	categoryService      pbc.CategoryServiceClient
	productService       pbc.ProductServiceClient
	clientService        pbu.ClientServiceClient
	courierService       pbu.CourierServiceClient
	userService          pbu.UserServiceClient
	branchService        pbu.BranchServiceClient
	authService          pbu.AuthServiceClient
	alternativeService   pbo.AlternativeServiceClient
	deliveryTarifService pbo.DeliveryTarifServicesClient
	orderService         pbo.OrderServiceClient
	orderProductService  pbo.OrderProductServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connUserService, err := grpc.Dial(
		cfg.UserGRPCServiceHost+cfg.UserGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connCatalogService, err := grpc.Dial(
		cfg.CatalogGRPCServiceHost+cfg.CatalogGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	connOrderService, err := grpc.Dial(
		cfg.OrderGRPCServiceHost+cfg.OrderGRPCServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err

	}
	return &grpcClients{
		// CATALOG SERVICE
		categoryService: pbc.NewCategoryServiceClient(connCatalogService),
		productService:  pbc.NewProductServiceClient(connCatalogService),

		// USER SERVICE
		clientService:  pbu.NewClientServiceClient(connUserService),
		courierService: pbu.NewCourierServiceClient(connUserService),
		userService:    pbu.NewUserServiceClient(connUserService),
		branchService:  pbu.NewBranchServiceClient(connUserService),
		authService:    pbu.NewAuthServiceClient(connUserService),

		// ORDER SERVICE
		alternativeService:   pbo.NewAlternativeServiceClient(connOrderService),
		deliveryTarifService: pbo.NewDeliveryTarifServicesClient(connOrderService),
		orderService:         pbo.NewOrderServiceClient(connOrderService),
		orderProductService:  pbo.NewOrderProductServiceClient(connOrderService),
	}, nil

}

// CATALOG SERVICE

func (g *grpcClients) CategoryService() pbc.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClients) ProductService() pbc.ProductServiceClient {
	return g.productService
}

// USER SERVICE

func (g *grpcClients) ClientService() pbu.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) CourierService() pbu.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) UserService() pbu.UserServiceClient {
	return g.userService
}

func (g *grpcClients) BranchService() pbu.BranchServiceClient {
	return g.branchService
}
 
func (g *grpcClients) AuthService() pbu.AuthServiceClient {
	return g.authService
}

// ORDER SERVICE

func (g *grpcClients) AlternativeService() pbo.AlternativeServiceClient {
	return g.alternativeService
}

func (g *grpcClients) DeliveryTarifService() pbo.DeliveryTarifServicesClient {
	return g.deliveryTarifService
}

func (g *grpcClients) OrderService() pbo.OrderServiceClient {
	return g.orderService
}

func (g *grpcClients) OrderProductService() pbo.OrderProductServiceClient {
	return g.orderProductService
}
