package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct {
	ServiceName string
	Environment string

	HTTPPort string

	UserGRPCServiceHost string
	UserGRPCServicePort string

	CatalogGRPCServiceHost string
	CatalogGRPCServicePort string

	OrderGRPCServiceHost string
	OrderGRPCServicePort string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("error!!!", err)
	}

	cfg := Config{}

	cfg.ServiceName = cast.ToString(getOrReturnDefault("SERVICE_NAME", "delivery"))
	cfg.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "dev"))

	cfg.HTTPPort = cast.ToString(getOrReturnDefault("HTTP_PORT", ":8080"))

	cfg.UserGRPCServiceHost = cast.ToString(getOrReturnDefault("USER_GRPC_SERVICE_HOST", "localhost"))
	cfg.UserGRPCServicePort = cast.ToString(getOrReturnDefault("USER_GRPC_SERVICE_PORT", ":8080"))

	cfg.CatalogGRPCServiceHost = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_HOST", "localhost"))
	cfg.CatalogGRPCServicePort = cast.ToString(getOrReturnDefault("CATALOG_GRPC_SERVICE_PORT", ":8080"))

	cfg.OrderGRPCServiceHost = cast.ToString(getOrReturnDefault("ORDER_GRPC_SERVICE_HOST", "localhost"))
	cfg.OrderGRPCServicePort = cast.ToString(getOrReturnDefault("ORDER_GRPC_SERVICE_PORT", ":8080"))

	return cfg
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	value := os.Getenv(key)
	if value != "" {
		return value
	}

	return defaultValue
}
