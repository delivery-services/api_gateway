package config

import (
	"errors"
	"time"
)

var SignKey = []byte("asd@#lskd2!aw32k34242WSASdsk32")

const (
	AccessExpireTime  = time.Minute * 20
	RefreshExpireTime = time.Hour * 24
)

const (
	// DebugMode indicates service mode is debug.
	DebugMode = "debug"
	// TestMode indicates service mode is test.
	TestMode = "test"
	// ReleaseMode indicates service mode is release.
	ReleaseMode = "release"
)

var ErrUnauthorized = errors.New("UNAUTHORITHED")