package handler

import (
	pbu "api_gateway/genproto/delivery-protos/user_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateBranch godoc
// @Router       /v1/branch [POST]
// @Summary      Create a new branch
// @Description  Create a new branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        branch body models.CreateBranch false "branch"
// @Success      201  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateBranch(c *gin.Context) {

	request := pbu.CreateBranch{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from branch", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.BranchService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating branch", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "branch created", http.StatusCreated, response)
}

// GetBranchByID godoc
// @Router       /v1/branch/{id} [GET]
// @Summary      Get branch by id
// @Description  Get branch by id
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch"
// @Success      200  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	branch, err := h.services.BranchService().GetByID(context.Background(), &pbu.GetBranchByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get branch by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, branch)

}

// GetBranchsList godoc
// @Router       /v1/branches [GET]
// @Summary      Get branches list
// @Description  Get branches list
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.BranchesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetBranchesList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.BranchService().GetAll(context.Background(), &pbu.GetBranchList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting branch", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateBranch godoc
// @Router       /v1/branch/{id} [PUT]
// @Summary      Update branch by id
// @Description  Update branch by id
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch id"
// @Param        branch body models.UpdateBranch true "branch"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateBranch(c *gin.Context) {

	request := pbu.UpdateBranch{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.BranchService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating branch", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteBranch godoc
// @Router       /v1/branch/{id} [DELETE]
// @Summary      Delete branch
// @Description  Delete branch
// @Tags         branch
// @Accept       json
// @Produce      json
// @Param        id path string true "branch id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteBranch(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.BranchService().Delete(context.Background(), &pbu.GetBranchByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting branch by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}