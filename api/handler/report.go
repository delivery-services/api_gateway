package handler

import (
	pbo "api_gateway/genproto/delivery-protos/order_service"
	"api_gateway/genproto/delivery-protos/user_service"
	"context"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// BranchReport godoc
// @Router       /v1/report [GET]
// @Summary      REPORT
// @Description  REPORT
// @Tags         report
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.Branch
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) BranchReport(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.OrderService().GetAll(context.Background(), &pbo.GetOrderList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting orders", http.StatusInternalServerError, err)
		return
	}

	branchCount := make(map[string]int)
	orders := response.Order

	tenDaysAgo := time.Now().AddDate(0, 0, -10)
	tenDaysAgoUnix := tenDaysAgo.Unix()

	for _, order := range orders {
		createdAtUnix, _ := time.Parse(time.RFC3339, order.CreatedAt)
		if createdAtUnix.Unix() >= tenDaysAgoUnix {
			branchID := order.BranchId
			branchCount[branchID]++
		}
	}
	var maxBranchID uuid.UUID
	maxCount := 0

	for branchID, count := range branchCount {

		if count > maxCount {
			maxBranchID, _ = uuid.Parse(branchID)
			maxCount = count
		}
	}

	branch, err := h.services.BranchService().GetByID(context.Background(), &user_service.GetBranchByID{
		Id: maxBranchID.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while getting max branch", http.StatusInternalServerError, err)

	}

	handleResponse(c, h.log, "", http.StatusOK, branch.Name)

}
