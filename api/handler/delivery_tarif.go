package handler

import (
	pbo "api_gateway/genproto/delivery-protos/order_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateDeliveryTarif godoc
// @Router       /v1/delivery_tarif [POST]
// @Summary      Create a new delivery_tarif
// @Description  Create a new delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        delivery_tarif body models.CreateDeliveryTarif false "delivery_tarif"
// @Success      201  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateDeliveryTarif(c *gin.Context) {

	request := pbo.CreateDeliveryTarif{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from delivery_tarif", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.DeliveryTarifService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating delivery_tarif", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "delivery_tarif created", http.StatusCreated, response)
}

// GetDeliveryTarifByID godoc
// @Router       /v1/delivery_tarif/{id} [GET]
// @Summary      Get delivery_tarif by id
// @Description  Get delivery_tarif by id
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tarif"
// @Success      200  {object}  models.DeliveryTarif
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarifByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	delivery_tarif, err := h.services.DeliveryTarifService().GetByID(context.Background(), &pbo.GetDeliveryTarifByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get delivery_tarif by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, delivery_tarif)

}

// GetDeliveryTarifsList godoc
// @Router       /v1/delivery_tarifs [GET]
// @Summary      Get delivery_tarifs list
// @Description  Get delivery_tarifs list
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.DeliveryTarifsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetDeliveryTarifsList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.DeliveryTarifService().GetAll(context.Background(), &pbo.GetDeliveryTarifList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting delivery_tarifs", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateDeliveryTarif godoc
// @Router       /v1/delivery_tarif/{id} [PUT]
// @Summary      Update delivery_tarif by id
// @Description  Update delivery_tarif by id
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tarif id"
// @Param        delivery_tarif body models.UpdateDeliveryTarif true "delivery_tarif"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateDeliveryTarif(c *gin.Context) {

	request := pbo.UpdateDeliveryTarif{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.DeliveryTarifService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating delivery_tarif", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteDeliveryTarif godoc
// @Router       /v1/delivery_tarif/{id} [DELETE]
// @Summary      Delete delivery_tarif
// @Description  Delete delivery_tarif
// @Tags         delivery_tarif
// @Accept       json
// @Produce      json
// @Param        id path string true "delivery_tarif id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteDeliveryTarif(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.DeliveryTarifService().Delete(context.Background(), &pbo.GetDeliveryTarifByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting delivery_tarif by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}