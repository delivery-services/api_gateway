package handler

import (
	pbu "api_gateway/genproto/delivery-protos/user_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateClient godoc
// @Router       /v1/client [POST]
// @Summary      Create a new client
// @Description  Create a new client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        client body models.CreateClient false "client"
// @Success      201  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateClient(c *gin.Context) {

	request := pbu.CreateClient{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.ClientService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating client", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "client created", http.StatusCreated, response)
}

// GetClientByID godoc
// @Router       /v1/client/{id} [GET]
// @Summary      Get client by id
// @Description  Get client by id
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "client"
// @Success      200  {object}  models.Client
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClientByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	client, err := h.services.ClientService().GetByID(context.Background(), &pbu.GetClientByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get client by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, client)

}

// GetClientsList godoc
// @Router       /v1/clients [GET]
// @Summary      Get clients list
// @Description  Get clients list
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.ClientsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetClientsList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.ClientService().GetAll(context.Background(), &pbu.GetClientList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting clients", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateClient godoc
// @Router       /v1/client/{id} [PUT]
// @Summary      Update client by id
// @Description  Update client by id
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "client id"
// @Param        client body models.UpdateClient true "client"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateClient(c *gin.Context) {

	request := pbu.UpdateClient{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.ClientService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating client", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteClient godoc
// @Router       /v1/client/{id} [DELETE]
// @Summary      Delete client
// @Description  Delete client
// @Tags         client
// @Accept       json
// @Produce      json
// @Param        id path string true "client id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteClient(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.ClientService().Delete(context.Background(), &pbu.GetClientByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting client by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}