package handler

import (
	pbu "api_gateway/genproto/delivery-protos/user_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateCourier godoc
// @Router       /v1/courier [POST]
// @Summary      Create a new courier
// @Description  Create a new courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        courier body models.CreateCourier false "courier"
// @Success      201  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCourier(c *gin.Context) {

	request := pbu.CreateCourier{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from courier", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.CourierService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating courier", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "courier created", http.StatusCreated, response)
}

// GetCourierByID godoc
// @Router       /v1/courier/{id} [GET]
// @Summary      Get courier by id
// @Description  Get courier by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier"
// @Success      200  {object}  models.Courier
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCourierByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	courier, err := h.services.CourierService().GetByID(context.Background(), &pbu.GetCourierByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get client by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, courier)

}

// GetCouriersList godoc
// @Router       /v1/couriers [GET]
// @Summary      Get couriers list
// @Description  Get couriers list
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.CouriersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCouriersList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.CourierService().GetAll(context.Background(), &pbu.GetCourierList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting couriers", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateCourier godoc
// @Router       /v1/courier/{id} [PUT]
// @Summary      Update courier by id
// @Description  Update courier by id
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier id"
// @Param        courier body models.UpdateCourier true "courier"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCourier(c *gin.Context) {

	request := pbu.UpdateCourier{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.CourierService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating courier", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteCourier godoc
// @Router       /v1/courier/{id} [DELETE]
// @Summary      Delete courier
// @Description  Delete courier
// @Tags         courier
// @Accept       json
// @Produce      json
// @Param        id path string true "courier id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCourier(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.CourierService().Delete(context.Background(), &pbu.GetCourierByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting courier by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}