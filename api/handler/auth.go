package handler

import (
	"api_gateway/api/models"
	"api_gateway/config"
	pbu "api_gateway/genproto/delivery-protos/user_service"
	"api_gateway/pkg/jwt"
	"api_gateway/pkg/logger"
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

// UserLogin godoc
// @Router       /v1/login [POST]
// @Summary      User Login
// @Description  User Login
// @Tags         auth
// @Accept       json
// @Produce      json
// @Param        login body models.UserLoginRequest false "login"
// @Success      201  {object}  models.UserLoginResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UserLogin(c *gin.Context) {
	var (
		loginRequest  = pbu.UserLoginRequest{}
		loginResponse = models.UserLoginResponse{}
	)

	if err := c.ShouldBindJSON(&loginRequest); err != nil {
		handleResponse(c, h.log, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.AuthService().UserLogin(ctx, &loginRequest)
	if err != nil {
		handleResponse(c, h.log, "error while customer login", http.StatusInternalServerError, err.Error())
		return
	}

	claims := map[string]interface{}{
		"user_id": resp.GetId(),
	}

	accessToken, err := jwt.GenerateJWT(claims, config.AccessExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, h.log, "error while generating jwt", http.StatusInternalServerError, logger.Error(err))
		return
	}

	refreshToken, err := jwt.GenerateJWT(claims, config.RefreshExpireTime, string(config.SignKey))
	if err != nil {
		handleResponse(c, h.log, "error while generating jwt", http.StatusInternalServerError, logger.Error(err))
		return
	}

	loginResponse.AccesToken = accessToken
	loginResponse.RefreshToken = refreshToken

	fmt.Println(accessToken)
	fmt.Println(refreshToken)

	handleResponse(c, h.log, "", http.StatusOK, loginResponse)
}

func (h Handler) GetAuthInfoFromToken(c *gin.Context) (models.AuthInfo, error) {
	accessToken := c.GetHeader("Authorization")
	if accessToken == "" {
		return models.AuthInfo{}, config.ErrUnauthorized
	}

	claims, err := jwt.ExtractClaims(accessToken, string(config.SignKey))
	if err != nil {
		return models.AuthInfo{}, err
	}

	userLogin := cast.ToString(claims["user_login"])

	return models.AuthInfo{
		UserLogin: userLogin,
	}, nil
}
