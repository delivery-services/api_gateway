package handler

import (
	pbo "api_gateway/genproto/delivery-protos/order_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateOrder godoc
// @Router       /v1/order [POST]
// @Summary      Create a new order
// @Description  Create a new order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        order body models.CreateOrder false "order"
// @Success      201  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrder(c *gin.Context) {

	request := pbo.CreateOrder{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from order", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.OrderService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating order", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "order created", http.StatusCreated, response)
}

// GetOrderByID godoc
// @Router       /v1/order/{id} [GET]
// @Summary      Get order by id
// @Description  Get order by id
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order"
// @Success      200  {object}  models.Order
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	order, err := h.services.OrderService().GetByID(context.Background(), &pbo.GetOrderByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get order by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, order)

}

// GetOrdersList godoc
// @Router       /v1/orders [GET]
// @Summary      Get orders list
// @Description  Get orders list
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.OrdersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrdersList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.OrderService().GetAll(context.Background(), &pbo.GetOrderList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting orders", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateOrder godoc
// @Router       /v1/order/{id} [PUT]
// @Summary      Update order by id
// @Description  Update order by id
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order id"
// @Param        order body models.UpdateOrder true "order"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrder(c *gin.Context) {

	request := pbo.UpdateOrder{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.OrderService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating order_product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteOrder godoc
// @Router       /v1/order/{id} [DELETE]
// @Summary      Delete order
// @Description  Delete order
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrder(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.OrderService().Delete(context.Background(), &pbo.GetOrderByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting order by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}

// UpdateOrderStatus godoc
// @Router       /v1/order_status/{id} [PATCH]
// @Summary      Update order status
// @Description  Update order status
// @Tags         order
// @Accept       json
// @Produce      json
// @Param        id path string true "order id"
// @Param        order body models.UpdateOrderStatus true "update order status"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrderStatus(c *gin.Context) {

	request := pbo.Order{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.OrderService().UpdateOrderStatus(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating order_product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}
