package handler

import (
	pbu "api_gateway/genproto/delivery-protos/user_service"
	"context"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateUser godoc
// @Router       /v1/user [POST]
// @Summary      Create a new user
// @Description  Create a new user
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        user body models.CreateUser false "user"
// @Success      201  {object}  models.User
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateUser(c *gin.Context) {

	request := pbu.CreateUser{}

	if err := c.ShouldBind(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	response, err := h.services.UserService().Create(ctx, &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating user", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "user created", http.StatusCreated, response)
}

// GetUserByID godoc
// @Router       /v1/user/{id} [GET]
// @Summary      Get user by id
// @Description  Get user by id
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        id path string true "user"
// @Success      200  {object}  models.User
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetUserByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	user, err := h.services.UserService().GetByID(context.Background(), &pbu.GetUserByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get user by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, user)

}

// GetUsersList godoc
// @Router       /v1/users [GET]
// @Summary      Get users list
// @Description  Get users list
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.UsersResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetUsersList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.UserService().GetAll(context.Background(), &pbu.GetUserList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting user", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateUser godoc
// @Router       /v1/user/{id} [PUT]
// @Summary      Update user by id
// @Description  Update user by id
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        id path string true "user id"
// @Param        user body models.UpdateUser true "user"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateUser(c *gin.Context) {

	request := pbu.UpdateUser{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.UserService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating user", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteUser godoc
// @Router       /v1/user/{id} [DELETE]
// @Summary      Delete user
// @Description  Delete user
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        id path string true "user id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteUser(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.UserService().Delete(context.Background(), &pbu.GetUserByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting user by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}

// UpdateUserPassword godoc
// @Security      ApiKeyAuth
// @Router       /v1/user/{id} [PATCH]
// @Summary      Update user password
// @Description  Update user password
// @Tags         user
// @Accept       json
// @Produce      json
// @Param        user body models.UpdatePasswordRequest true "user"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateUserPassword(c *gin.Context) {

	// if _, err := h.GetAuthInfoFromToken(c); err != nil {
	// 	handleResponse(c, h.log, "Unauthorized", 401, config.ErrUnauthorized)
	// 	return
	// }

	updateUserPassword := pbu.UpdateUserPasswordRequest{}

	if err := c.ShouldBindJSON(&updateUserPassword); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	fmt.Println("login", updateUserPassword.Login)
	fmt.Println("old passowrd", updateUserPassword.OldPassword)
	fmt.Println("new passowrd", updateUserPassword.NewPassword)

	_, err := h.services.UserService().UpdatePassword(context.Background(), &updateUserPassword)
	if err != nil {
		handleResponse(c, h.log, "error while updating user password by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "password successfully updated")
}
