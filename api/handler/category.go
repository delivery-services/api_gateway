package handler

import (
	pbc "api_gateway/genproto/delivery-protos/catalog_service"

	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateCategory godoc
// @Router       /v1/category [POST]
// @Summary      Create a new category
// @Description  Create a new category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        category body models.CreateCategory false "category"
// @Success      201  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCategory(c *gin.Context) {

	request := pbc.CreateCategory{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from category", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.CategoryService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating category", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "category created", http.StatusCreated, response)
}

// GetCategoryByID godoc
// @Router       /v1/category/{id} [GET]
// @Summary      Get category by id
// @Description  Get category by id
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category"
// @Success      200  {object}  models.Category
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategoryByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	category, err := h.services.CategoryService().GetByID(context.Background(), &pbc.GetCategoryByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get category by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, category)

}

// GetCateogriesList godoc
// @Router       /v1/categories [GET]
// @Summary      Get categories list
// @Description  Get categories list
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.CategoriesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetCategoriesList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.CategoryService().GetAll(context.Background(), &pbc.GetCategoryList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting category", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateCategory godoc
// @Router       /v1/category/{id} [PUT]
// @Summary      Update category by id
// @Description  Update category by id
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category id"
// @Param        category body models.UpdateCategory true "category"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateCategory(c *gin.Context) {

	request := pbc.UpdateCategory{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.CategoryService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating category", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteCategory godoc
// @Router       /v1/category/{id} [DELETE]
// @Summary      Delete category
// @Description  Delete category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        id path string true "category id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteCategory(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.CategoryService().Delete(context.Background(), &pbc.GetCategoryByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting category by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}