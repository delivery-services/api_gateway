package handler

import (
	pbc "api_gateway/genproto/delivery-protos/catalog_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateProduct godoc
// @Router       /v1/product [POST]
// @Summary      Create a new product
// @Description  Create a new product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        product body models.CreateProduct false "product"
// @Success      201  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateProduct(c *gin.Context) {
  
	request := pbc.CreateProduct{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from product", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.ProductService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "product created", http.StatusCreated, response)
}

// GetProductByID godoc
// @Router       /v1/product/{id} [GET]
// @Summary      Get product by id
// @Description  Get product by id
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product"
// @Success      200  {object}  models.Product
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProductByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	product, err := h.services.ProductService().GetByID(context.Background(), &pbc.GetProductByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get product by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, product)

}

// GetProductsList godoc
// @Router       /v1/products [GET]
// @Summary      Get products list
// @Description  Get products list
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.ProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetProductsList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.ProductService().GetAll(context.Background(), &pbc.GetProductList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting products", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateProduct godoc
// @Router       /v1/product/{id} [PUT]
// @Summary      Update product by id
// @Description  Update product by id
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product id"
// @Param        product body models.UpdateProduct true "product"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateProduct(c *gin.Context) {

	request := pbc.UpdateProduct{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.ProductService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteProduct godoc
// @Router       /v1/product/{id} [DELETE]
// @Summary      Delete product
// @Description  Delete product
// @Tags         product
// @Accept       json
// @Produce      json
// @Param        id path string true "product id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteProduct(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.ProductService().Delete(context.Background(), &pbc.GetProductByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}