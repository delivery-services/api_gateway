package handler

import (
	pbo "api_gateway/genproto/delivery-protos/order_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateOrderProduct godoc
// @Router       /v1/order_product [POST]
// @Summary      Create a new order_product
// @Description  Create a new order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        order_product body models.CreateOrderProduct false "order_product"
// @Success      201  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateOrderProduct(c *gin.Context) {

	request := pbo.CreateOrderProduct{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from order_product", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.OrderProductService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating order_product", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "order_product created", http.StatusCreated, response)
}

// GetOrderProductByID godoc
// @Router       /v1/order_product/{id} [GET]
// @Summary      Get order_product by id
// @Description  Get order_product by id
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product"
// @Success      200  {object}  models.OrderProduct
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProductByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	order_product, err := h.services.OrderProductService().GetByID(context.Background(), &pbo.GetOrderProductByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get order_product by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, order_product)

}

// GetOrderProductsList godoc
// @Router       /v1/order_products [GET]
// @Summary      Get order_products list
// @Description  Get order_products list
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.OrderProductsResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetOrderProductsList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.OrderProductService().GetAll(context.Background(), &pbo.GetOrderProductList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting order_products", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateOrderProduct godoc
// @Router       /v1/order_product/{id} [PUT]
// @Summary      Update order_product by id
// @Description  Update order_product by id
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product id"
// @Param        order_product body models.UpdateOrderProduct true "order_product"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateOrderProduct(c *gin.Context) {

	request := pbo.UpdateOrderProduct{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.OrderProductService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating order_product", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteOrderProduct godoc
// @Router       /v1/order_product/{id} [DELETE]
// @Summary      Delete order_product
// @Description  Delete order_product
// @Tags         order_product
// @Accept       json
// @Produce      json
// @Param        id path string true "order_product id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteOrderProduct(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.OrderProductService().Delete(context.Background(), &pbo.GetOrderProductByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting order_product by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}