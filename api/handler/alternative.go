package handler

import (
	pbo "api_gateway/genproto/delivery-protos/order_service"
	"context"
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

// CreateAlternative godoc
// @Router       /v1/alternative [POST]
// @Summary      Create a new alternative
// @Description  Create a new alternative
// @Tags         alternative
// @Accept       json
// @Produce      json
// @Param        alternative body models.CreateAlternative false "alternative"
// @Success      201  {object}  models.Alternative
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateAlternative(c *gin.Context) {

	request := pbo.CreateAlternative{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from alternative", http.StatusBadRequest, err)
		return
	}

	response, err := h.services.AlternativeService().Create(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating alternative", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "alternative created", http.StatusCreated, response)
}

// GetAlternativeByID godoc
// @Router       /v1/alternative/{id} [GET]
// @Summary      Get alternative by id
// @Description  Get alternative by id
// @Tags         alternative
// @Accept       json
// @Produce      json
// @Param        id path string true "alternative"
// @Success      200  {object}  models.Alternative
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAlternativeByID(c *gin.Context) {

	var err error

	uid := c.Param("id")

	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "invalid uuid type ", http.StatusBadRequest, err.Error())
		return
	}

	alternative, err := h.services.AlternativeService().GetByID(context.Background(), &pbo.GetAlternativeByID{
		Id: id.String(),
	})
	if err != nil {
		handleResponse(c, h.log, "error while get user by id", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, alternative)

}

// GetAlternativesList godoc
// @Router       /v1/alternatives [GET]
// @Summary      Get alternatives list
// @Description  Get alternatives list
// @Tags         alternative
// @Accept       json
// @Produce      json
// @Param        page query string false "page"
// @Param        limit query string false "limit"
// @Param        search query string false "search"
// @Success      200  {object}  models.AlternativesResponse
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) GetAlternativesList(c *gin.Context) {

	var (
		page, limit int
		search      string
		err         error
	)

	pageStr := c.DefaultQuery("page", "1")
	page, err = strconv.Atoi(pageStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing page ", http.StatusBadRequest, err.Error())
		return
	}

	limitStr := c.DefaultQuery("limit", "100")
	limit, err = strconv.Atoi(limitStr)
	if err != nil {
		handleResponse(c, h.log, "error while parsing limit", http.StatusBadRequest, err.Error())
		return
	}

	search = c.Query("search")

	response, err := h.services.AlternativeService().GetAll(context.Background(), &pbo.GetAlternativeList{
		Page:   int32(page),
		Limit:  int32(limit),
		Search: search,
	})

	if err != nil {
		handleResponse(c, h.log, "error while getting user", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, response)

}

// UpdateAlternative godoc
// @Router       /v1/alternative/{id} [PUT]
// @Summary      Update alternative by id
// @Description  Update alternative by id
// @Tags         alternative
// @Accept       json
// @Produce      json
// @Param        id path string true "alternative id"
// @Param        user body models.UpdateAlternative true "user"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) UpdateAlternative(c *gin.Context) {

	request := pbo.UpdateAlternative{}

	uid := c.Param("id")
	if uid == "" {
		handleResponse(c, h.log, "invalid uuid", http.StatusBadRequest, errors.New("uuid is not valid"))
		return
	}

	request.Id = uid

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body", http.StatusBadRequest, err.Error())
		return
	}

	id, err := h.services.AlternativeService().Update(context.Background(), &request)
	if err != nil {
		handleResponse(c, h.log, "error while updating alternative", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, id)

}

// DeleteAlternative godoc
// @Router       /v1/alternative/{id} [DELETE]
// @Summary      Delete alternative
// @Description  Delete alternative
// @Tags         alternative
// @Accept       json
// @Produce      json
// @Param        id path string true "alternative id"
// @Success      200  {object}  models.Response
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) DeleteAlternative(c *gin.Context) {

	uid := c.Param("id")
	id, err := uuid.Parse(uid)
	if err != nil {
		handleResponse(c, h.log, "uuid is not valid", http.StatusBadRequest, err.Error())
		return
	}

	if _, err := h.services.AlternativeService().Delete(context.Background(), &pbo.GetAlternativeByID{
		Id: id.String(),
	}); err != nil {
		handleResponse(c, h.log, "error while deleting alternative by id", http.StatusInternalServerError, err.Error())
		return
	}

	handleResponse(c, h.log, "", http.StatusOK, "data succesfully deleted")

}