package api

import (
	"api_gateway/api/handler"

	cors "github.com/itsjamie/gin-cors"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "api_gateway/api/docs"

	"github.com/gin-gonic/gin"
)

// New ...
// @title           DELIVERY SERVICE
// @version         1.0
// @description     delivery service API
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

	r.Use(cors.Middleware(cors.Config{
		Origins:        "*",
		RequestHeaders: "Authorization, Origin, Content-Type",
		Methods:        "POST, GET, PUT, DELETE, OPTION",
	}))

	v1Route := r.Group("/v1")

	// AUTH ENDPOINTS

	v1Route.POST("login", h.UserLogin)

	// ALTERNATIVE ENDPOINTS
	v1Route.POST("alternative", h.CreateAlternative)
	v1Route.GET("alternative/:id", h.GetAlternativeByID)
	v1Route.GET("alternatives", h.GetAlternativesList)
	v1Route.PUT("alternative/:id", h.UpdateAlternative)
	v1Route.DELETE("alternative/:id", h.DeleteAlternative)

	// USER ENDPOINTS
	v1Route.POST("/user", h.CreateUser)
	v1Route.GET("/user/:id", h.GetUserByID)
	v1Route.GET("/users", h.GetUsersList)
	v1Route.PUT("/user/:id", h.UpdateUser)
	v1Route.DELETE("/user/:id", h.DeleteUser)
	v1Route.PATCH("/user/:id", h.UpdateUserPassword)

	// Category endpoints
	v1Route.POST("category", h.CreateCategory)
	v1Route.GET("category/:id", h.GetCategoryByID)
	v1Route.GET("categories", h.GetCategoriesList)
	v1Route.PUT("category/:id", h.UpdateCategory)
	v1Route.DELETE("category/:id", h.DeleteCategory)

	// Product endpoints
	v1Route.POST("product", h.CreateProduct)
	v1Route.GET("product/:id", h.GetProductByID)
	v1Route.GET("products", h.GetProductsList)
	v1Route.PUT("product/:id", h.UpdateProduct)
	v1Route.DELETE("product/:id", h.DeleteProduct)

	// Order endpoints
	v1Route.POST("order", h.CreateOrder)
	v1Route.GET("order/:id", h.GetOrderByID)
	v1Route.GET("orders", h.GetOrdersList)
	v1Route.PUT("order/:id", h.UpdateOrder)
	v1Route.DELETE("order/:id", h.DeleteOrder)

	// Client endpoints
	v1Route.POST("client", h.CreateClient)
	v1Route.GET("client/:id", h.GetClientByID)
	v1Route.GET("clients", h.GetClientsList)
	v1Route.PUT("client/:id", h.UpdateClient)
	v1Route.DELETE("client/:id", h.DeleteClient)

	// Branch endpoints
	v1Route.POST("branch", h.CreateBranch)
	v1Route.GET("branch/:id", h.GetBranchByID)
	v1Route.GET("branches", h.GetBranchesList)
	v1Route.PUT("branch/:id", h.UpdateBranch)
	v1Route.DELETE("branch/:id", h.DeleteBranch)

	// Courier endpoints
	v1Route.POST("courier", h.CreateCourier)
	v1Route.GET("courier/:id", h.GetCourierByID)
	v1Route.GET("couriers", h.GetCouriersList)
	v1Route.PUT("courier/:id", h.UpdateCourier)
	v1Route.DELETE("courier/:id", h.DeleteCourier)

	// DeliveryTarif endpoints
	v1Route.POST("delivery_tarif", h.CreateDeliveryTarif)
	v1Route.GET("delivery_tarif/:id", h.GetDeliveryTarifByID)
	v1Route.GET("delivery_tarifs", h.GetDeliveryTarifsList)
	v1Route.PUT("delivery_tarif/:id", h.UpdateDeliveryTarif)
	v1Route.DELETE("delivery_tarif/:id", h.DeleteDeliveryTarif)

	// OrderProduct endpoints
	v1Route.POST("order_product", h.CreateOrderProduct)
	v1Route.GET("order_product/:id", h.GetOrderProductByID)
	v1Route.GET("order_products", h.GetOrderProductsList)
	v1Route.PUT("order_product/:id", h.UpdateOrderProduct)
	v1Route.DELETE("order_product/:id", h.DeleteOrderProduct)

	// active_branches
	v1Route.GET("get_active_branches", h.GetActiveBranchesList)

	// update_order_status
	v1Route.PATCH("order_status/:id", h.UpdateOrderStatus)

	// REPORT
	v1Route.GET("report", h.BranchReport)

	// Swagger UI
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
