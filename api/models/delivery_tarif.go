package models

type DeliveryTarif struct {
	ID            string `json:"id"`
	Name          string `json:"name"`
	Type          string `json:"type"`
	BasePrice     int32  `json:"base_price"`
	AlternativeID string `json:"alternative_id"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
	DeletedAt     int32  `json:"deleted_at"`
}

type CreateDeliveryTarif struct {
	Name      string `json:"name"`
	Type      string `json:"type"`
	BasePrice int32  `json:"base_price"`
	AlternativeID string `json:"alternative_id"`
}

type DeliveryTarifsResponse struct {
	DeliveryTarifs []DeliveryTarif `json:"delivery_tarifs"`
	Count          int32           `json:"count"`
}

type UpdateDeliveryTarif struct {
	ID            string `json:"-"`
	Name          string `json:"name"`
	Type          string `json:"type"`
	BasePrice     int32  `json:"base_price"`
	AlternativeID string `json:"alternative_id"`
}
