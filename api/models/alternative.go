package models

type Alternative struct {
	ID              string  `json:"id"`
	FromPrice       float32 `json:"from_price"`
	ToPrice         float32 `json:"to_price"`
	Price           float32 `json:"price"`
	CreatedAt       string  `json:"created_at"`
	UpdatedAt       string  `json:"updated_at"`
	DeletedAt       int32   `json:"deleted_at"`
}

type CreateAlternative struct {
	FromPrice       float32 `json:"from_price"`
	ToPrice         float32 `json:"to_price"`
	Price           float32 `json:"price"`
}

type AlternativesResponse struct {
	Alternatives []Alternative `json:"alternatives"`
	Count        int32         `json:"count"`
}

type UpdateAlternative struct {
	ID              string  `json:"-"`
	FromPrice       float32 `json:"from_price"`
	ToPrice         float32 `json:"to_price"`
	Price           float32 `json:"price"`
}
