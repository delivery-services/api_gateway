package models

type Product struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Photo       string `json:"photo"`
	OrderNumber string `json:"order_number"`
	Active      string `json:"active"`
	Type        string `json:"type"`
	Price       int32  `json:"price"`
	CategoryID  string `json:"category_id"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
	DeletedAt   int32  `json:"deleted_at"`
}

type CreateProduct struct {
	Title       string `json:"title"`
	Description string `json:"description"`
	Photo       string `json:"photo"`
	OrderNumber string `json:"order_number"`
	Active      string `json:"active"`
	Type        string `json:"type"`
	Price       int32  `json:"price"`
	CategoryID  string `json:"category_id"`
}

type ProductsResponse struct {
	Products []Product `json:"products"`
	Count    int32     `json:"count"`
}

type UpdateProduct struct {
	ID          string `json:"-"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Photo       string `json:"photo"`
	OrderNumber string `json:"order_number"`
	Active      string `json:"active"`
	Type        string `json:"type"`
	Price       int32  `json:"price"`
	CategoryID  string `json:"category_id"`
}
