package models

type Category struct {
	ID          string `json:"id"`
	Title       string `json:"title"`
	Image       string `json:"image"`
	Active      string `json:"active"`
	CategoryID  string `json:"category_id"`
	OrderNumber string `json:"order_number"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
	DeletedAt   int32  `json:"deleted_at"`
}

type CreateCategory struct {
	Title       string `json:"title"`
	Image       string `json:"image"`
	Active      string `json:"active"`
	CategoryID  string `json:"category_id"`
	OrderNumber string `json:"order_number"`
}

type CategoriesResponse struct {
	Categories []Category `json:"categories"`
	Count      int32      `json:"count"`
}

type UpdateCategory struct {
	ID          string `json:"-"`
	Title       string `json:"title"`
	Image       string `json:"image"`
	Active      string `json:"active"`
	CategoryID  string `json:"category_id"`
	OrderNumber string `json:"order_number"`
}
