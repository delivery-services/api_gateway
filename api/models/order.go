package models

type Order struct {
	ID             string `json:"id"`
	OrderID        string `json:"order_id"`
	ClientID       string `json:"client_id"`
	BranchID       string `json:"branch_id"`
	Type           string `json:"type"`
	Address        string `json:"address"`
	CourierID      string `json:"courier_id"`
	Price          int32  `json:"price"`
	DeliveryPrice  int32  `json:"delivery_price"`
	Discount       int32  `json:"discount"`
	Status         string `json:"status"`
	PaymentType    string `json:"payment_type"`
	OrderProductID string `json:"order_product_id"`
	CreatedAt      string `json:"created_at"`
	UpdatedAt      string `json:"updated_at"`
	DeletedAt      int32  `json:"deleted_at"`
}

type CreateOrder struct {
	ClientID       string `json:"client_id"`
	BranchID       string `json:"branch_id"`
	Type           string `json:"type"`
	Address        string `json:"address"`
	CourierID      string `json:"courier_id"`
	Price          int32  `json:"price"`
	DeliveryPrice  int32  `json:"delivery_price"`
	Discount       int32  `json:"discount"`
	Status         string `json:"status"`
	PaymentType    string `json:"payment_type"`
	OrderProductID string `json:"order_product_id"`
}

type OrdersResponse struct {
	Orders []Order `json:"orders"`
	Count  int32   `json:"count"`
}

type UpdateOrder struct {
	ID             string `json:"-"`
	OrderID        string `json:"order_id"`
	ClientID       string `json:"client_id"`
	BranchID       string `json:"branch_id"`
	Type           string `json:"type"`
	Address        string `json:"address"`
	CourierID      string `json:"courier_id"`
	Price          int32  `json:"price"`
	DeliveryPrice  int32  `json:"delivery_price"`
	Discount       int32  `json:"discount"`
	Status         string `json:"status"`
	PaymentType    string `json:"payment_type"`
	OrderProductID string `json:"order_product_id"`
}

type UpdateOrderStatus struct {
	ID     string `json:"-"`
	Status string `json:"status"`
}
