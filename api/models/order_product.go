package models

type OrderProduct struct {
	ID        string `json:"id"`
	ProductID string `json:"product_id"`
	Quantity  int32  `json:"quantity"`
	Price     int32  `json:"price"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
	DeletedAt int32  `json:"deleted_at"`
}

type CreateOrderProduct struct {
	ProductID string `json:"product_id"`
	Quantity  int32  `json:"quantity"`
	Price     int32  `json:"price"`
}

type OrderProductsResponse struct {
	OrderProducts []OrderProduct `json:"order_products"`
	Count         int32          `json:"count"`
}

type UpdateOrderProduct struct {
	ID        string `json:"-"`
	ProductID string `json:"product_id"`
	Quantity  int32  `json:"quantity"`
	Price     int32  `json:"price"`
}
