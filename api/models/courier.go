package models

type Courier struct {
	ID            string `json:"id"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	DateOfBirth   string `json:"date_of_birth"`
	Age           int32  `json:"age"`
	Gender        string `json:"gender"`
	Active        string `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int32  `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
	CreatedAt     string `json:"created_at"`
	UpdatedAt     string `json:"updated_at"`
	DeletedAt     int32  `json:"deleted_at"`
}

type CreateCourier struct {
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	DateOfBirth   string `json:"date_of_birth"`
	Gender        string `json:"gender"`
	Active        string `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int32  `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
}

type CouriersResponse struct {
	Couriers []Courier `json:"couriers"`
	Count    int32     `json:"count"`
}

type UpdateCourier struct {
	ID            string `json:"-"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Phone         string `json:"phone"`
	DateOfBirth   string `json:"date_of_birth"`
	Age           int32  `json:"age"`
	Gender        string `json:"gender"`
	Active        string `json:"active"`
	Login         string `json:"login"`
	Password      string `json:"password"`
	MaxOrderCount int32  `json:"max_order_count"`
	BranchID      string `json:"branch_id"`
}
