package models

type Client struct {
	ID               string `json:"id"`
	FirstName        string `json:"first_name"`
	LastName         string `json:"last_name"`
	Phone            string `json:"phone"`
	Photo            string `json:"photo"`
	DateOfBirth      string `json:"date_of_birth"`
	Age              int32  `json:"age"`
	Gender           string `json:"gender"`
	LastOrderedDate  string `json:"last_ordered_date"`
	TotalOrdersSum   int32  `json:"total_orders_sum"`
	TotalOrdersCount int32  `json:"total_orders_count"`
	DiscountType     string `json:"discount_type"`
	DiscountAmount   int32  `json:"discount_amount"`
	CreatedAt        string `json:"created_at"`
	UpdatedAt        string `json:"updated_at"`
	DeletedAt        int32  `json:"deleted_at"`
}

type CreateClient struct {
	FirstName        string `json:"first_name"`
	LastName         string `json:"last_name"`
	Phone            string `json:"phone"`
	Photo            string `json:"photo"`
	DateOfBirth      string `json:"date_of_birth"`
	Gender           string `json:"gender"`
	LastOrderedDate  string `json:"last_ordered_date"`
	TotalOrdersSum   int32  `json:"total_orders_sum"`
	TotalOrdersCount int32  `json:"total_orders_count"`
	DiscountType     string `json:"discount_type"`
	DiscountAmount   int32  `json:"discount_amount"`
}

type ClientsResponse struct {
	Clients []Client `json:"clients"`
	Count   int32    `json:"count"`
}

type UpdateClient struct {
	ID               string `json:"-"`
	FirstName        string `json:"first_name"`
	LastName         string `json:"last_name"`
	Phone            string `json:"phone"`
	Photo            string `json:"photo"`
	DateOfBirth      string `json:"date_of_birth"`
	Age              int32  `json:"age"`
	Gender           string `json:"gender"`
	LastOrderedDate  string `json:"last_ordered_date"`
	TotalOrdersSum   int32  `json:"total_orders_sum"`
	TotalOrdersCount int32  `json:"total_orders_count"`
	DiscountType     string `json:"discount_type"`
	DiscountAmount   int32  `json:"discount_amount"`
}
