package models

type UserLoginRequest struct {
	Login    string `json:"login"`
	Passowrd string `json:"password"`
}

type UserLoginResponse struct {
	AccesToken   string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}

type AuthInfo struct {
	UserLogin string `json:"user_login"`
}
