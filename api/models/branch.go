package models

type Branch struct {
	ID              string `json:"id"`
	Name            string `json:"name"`
	Phone           string `json:"phone"`
	Photo           string `json:"photo"`
	DeliveryTarifID string `json:"delivery_tarif_id"`
	WorkingHour     string `json:"working_hour"`
	Address         string `json:"address"`
	Destination     string `json:"destination"`
	Active          string `json:"active"`
	CreatedAt       string `json:"created_at"`
	UpdatedAt       string `json:"updated_at"`
	DeletedAt       int32  `json:"deleted_at"`
}

type CreateBranch struct {
	Name            string `json:"name"`
	Phone           string `json:"phone"`
	Photo           string `json:"photo"`
	DeliveryTarifID string `json:"delivery_tarif_id"`
	WorkingHour     string `json:"working_hour"`
	Address         string `json:"address"`
	Destination     string `json:"destination"`
	Active          string `json:"active"`
}

type BranchesResponse struct {
	Branches []Branch `json:"branches"`
	Count    int32    `json:"count"`
}

type UpdateBranch struct {
	ID              string `json:"-"`
	Name            string `json:"name"`
	Phone           string `json:"phone"`
	Photo           string `json:"photo"`
	DeliveryTarifID string `json:"delivery_tarif_id"`
	WorkingHour     string `json:"working_hour"`
	Address         string `json:"address"`
	Destination     string `json:"destination"`
	Active          string `json:"active"`
}
