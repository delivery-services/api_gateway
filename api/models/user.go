package models

type User struct {
	ID          string `json:"id"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Phone       string `json:"phone"`
	DateOfBirth string `json:"date_of_birth"`
	Gender      string `json:"gender"`
	Active      string `json:"active"`
	Login       string `json:"login"`
	Password    string `json:"password"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
	DeletedAt   int32  `json:"deleted_at"`
}

type CreateUser struct {
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Phone       string `json:"phone"`
	DateOfBirth string `json:"date_of_birth"`
	Gender      string `json:"gender"`
	Active      string `json:"active"`
	Login       string `json:"login"`
	Password    string `json:"password"`
}

type UsersResponse struct {
	Users []User `json:"users"`
	Count int32  `json:"count"`
}

type UpdateUser struct {
	ID          string `json:"-"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	Phone       string `json:"phone"`
	DateOfBirth string `json:"date_of_birth"`
	Gender      string `json:"gender"`
	Active      string `json:"active"`
	Login       string `json:"login"`
	Password    string `json:"password"`
}